const { response } = require('express');
const e = require('express');
const express = require('express');

// Mongoose is a package that allows us to create Schemas to model our data structure
// and to manipulate our database using different access methods.
const mongoose = require('mongoose');
const port = 9090;
const app = express()

 //[SECTION] MongoDB connection
    //Syntax:
    //mongoose.connect("mongoDBconnection", {
     //option to avoid errors in our connection
    //})

    mongoose.connect("mongodb+srv://admin:admin@batch245-cervantes.ttw2wvw.mongodb.net/s35-discussion?retryWrites=true&w=majority",
    {   
        //allows us to avoid any current and future errors while connecting to MONGODB
        useNewUrlParser: true,
        useUnifiedTopology: true
    })

    let db = mongoose.connection;

    // error handling in connecting
    db.on("error", ()=>console.log(error));

    //this will be triggered if the connection is succesful
    db.once("open", () => console.log("We're connected to cloud database!"))


    //Mongoose Schemeas 
      //Schemas determine the structure of the dpocuments to be written
      //in the database schemas act as the blueprint to our database
      //syntax;
      //required is used to specify that a field must not be empty

      //default = is used if a field value is not supplied
      
      const taskSchema = new mongoose.Schema({
        name: {
            type: String,
            required: [true, "Task name is required!"]
        },
        status: {
            type: String,
            default: "pending"
        }
      })

      const userSchema = new mongoose.Schema({
        username: {
          type: String,
          required: [true, "username is required!"]
        },
        password: {
          type: String,
          required: [true, "password is required!"]
        }
      })

      //[SECTION] Models
      // Uses schema to create/instatiate documents/ objects that follows our schema structure

      //the variables/object that will be created can be used to run commands with our database

      // Syntax
      //const variableName = mongoose.model("collectionName", schemaName);
      const User = mongoose.model("User", userSchema)
  
       const Task = mongoose.model("Task", taskSchema)
       //middleware
       app.use(express.json())// allows the app to read json data
       app.use(express.urlencoded({extended: true}))// it will allow our app to read data from forms.
      
      

       //SECTION ROUTING
          //Create/add new task
          //1. check if the task is existing.
          //if task already exist in the database, we will return a message "The task is already exist"
          //if the task doesnt exits in the database, we will add it in the database.

          app.post('/task', (req,res) => {
            let input = req.body

            console.log(input.status);
            if(input.status === undefined){
              Task.findOne({
                name:input.name
               },(error, result)=>{
                console.log(result)
                if(result !== null){
                  return res.send("The task already existing")
                }else {
                  let newTask = new Task({
                    name: input.name
                  })
                  // save() method will save the object in the collection that the object instatiated
                  newTask.save((saveError, savedTask) => {
                    if(saveError){
                      console.log(saveError)
                    }else {
                      return res.send("New Task created!");
                    }
                  })
                }
                })
            } else {
              Task.findOne({
                name:input.name
               },(error, result)=>{
                console.log(result)
                if(result !== null){
                  return res.send("The task already existing")
                }else {
                  let newTask = new Task({
                    name: input.name,
                    status: input.status
                  })
                  // save() method will save the object in the collection that the object instatiated
                  newTask.save((saveError, savedTask) => {
                    if(saveError){
                      console.log(saveError)
                    }else {
                      return res.send("New Task created!");
                    }
                  })
                }
                })
            }
          })

          //[SECTION] CREATE USER
          app.post('/signup',(req,res)=>{
            let inputUser = req.body
            if(inputUser.username === undefined || inputUser.password === undefined){
                 console.log("Please input username or password!!!")
                 res.send("Please input username or password!!!")
            } else {
              User.findOne({
                username:inputUser.username,
                password:inputUser.password
              },(error, result)=>{
                if(error){
                  console.log(error)
                }else {
                  let newUser = new User({
                    username:inputUser.username,
                    password:inputUser.password
                  })
                  newUser.save((saveError, savedUser)=>{
                    if(saveError){
                      console.log(error)
                    } else {
                      return res.send("New user registered!!!")
                    }
                  })
                }
              })
            }
          })

          //[SECTION] Retrieving the all the tasks
            app.get('/task', (req,res) =>{
              Task.find({},(error, result)=>{
                if(error){
                  console.log(error);
                }else{
                  return res.send(result);
                }
              })
            })
          
            app.get('/users', (req,res)=>{
              User.find({},(error,result)=>{
                if(error){
                  console.log(error)
                }else{
                  return res.send(result)
                }
              })
            })
  
    app.listen(port, () => console.log(`Server is running at port ${port}`))